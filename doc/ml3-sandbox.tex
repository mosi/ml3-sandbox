\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{xcolor}
\usepackage[scaled=.85]{beramono}
\usepackage[T1]{fontenc}
\usepackage{cite}

\author{Oliver Reinhardt\\Modeling ans Simulation Group\\University of Rostock\\\texttt{oliver.reinhardt@uni-rostock.de}}
\title{ML3-Sandbox -- User Guide (v0.0.3-SNAPSHOT)}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstdefinestyle{sessl}{%
	escapeinside=||,
	language=scala,
	showstringspaces=false,
	%columns=flexible,
	basicstyle={\ttfamily},
	%	numbers=left,
	numberstyle=\tiny\color{gray},
	keywordstyle=\color{blue},
	commentstyle=\color{dkgreen},
	stringstyle=\color{mauve},
	breaklines=true,
	breakatwhitespace=true,
	tabsize=2
}

\lstset{style=sessl}

\begin{document}
\maketitle
\tableofcontents

\section*{Disclaimer}
ML3, the ML3 Sandbox and this User Guide are prototypes and subject to continuing development. Things will go wrong. When you encounter any issues, please contact the author of this guide to solve them.

\section{Overview}
The ML3 Sandbox allows to implement ML3 models and certain kinds of SESSL-ML3 experiments on those models. It provides editors for ML3 and SESSL, and can compile and execute the SESSL experiments. \autoref{fig:screenshot} shows the GUI.

To run the Sandbox you need to have Java Runtime Environment 8 (\url{https://www.java.com}). Simply double-click on \texttt{ml3-sandbox.jar} or execute \texttt{java -jar ml3-sandbox.jar}.

\begin{figure}[h]
\includegraphics[width=\textwidth]{figures/annotated-screenshot.pdf}
\caption{A screenshot of the ML3 sandbox: (1) the ML3 editor pane, (2) the SESSL editor pane, (3) the log pane, (4) the Model menu, (5) the Experiment menu.}\label{fig:screenshot}
\end{figure}

The Sandbox comes with several example models and experiments that can be found in the \texttt{examples}-directory.

\section{Writing a Model}

The ML3 model itself is written in the left text editor panel. Models can be created, opened and saved using the options in the Model menu in the main menu bar. The editor supports the model creation with syntax-highlighting and highlighting of syntactical errors.

An introduction into the ML3 modeling language is given in \cite{Warnke2015}. A few example models are distributed together with the ML3 Sandbox. Questions regarding ML3 can be asked to the author of this user guide via email.


\section{Performing Experiments}

The ML3 Sandbox does not only allow to write a model, but also to execute execute experiments using SESSL \cite{Ewald2014} and the SESSL-ML3 binding. SESSL experiments can be specified in the right editor panel. General documentation on SESSL is available at \url{https://git.informatik.uni-rostock.de/mosi/sessl/wikis/home}.

\subsection{Model Parameters}
ML3 supports two kinds of model parameters: parameter constants and maps. Constants can be set (and scanned) as the SESSL user guide describes it. Maps are, however, unique to ML3. The SESSL-ML3 binding allows to read parameter maps from CSV files using the command \lstinline|fromFile|, when the trait \lstinline|ParameterMaps| is added to the experiment. It needs two parameters: the location of the file (the path can be absolute, or relative to the Sandbox) and the name of the parameter:
\begin{lstlisting}
fromFile("examples/sir/infectionScaling.csv")("infectionScaling")
\end{lstlisting}
The CSV file need to have the index of the map (in this case age intervals) as its first column and the values as a column that has the same name as the map in the CSV header. The file to this example is distributed together with one of the example models.

\subsection{Initial States}
Creating an initial state for a ML3 model is still an open issue. Currently the SESSL-ML3 binding supports three ways to get an initial state:
\begin{enumerate}
\item The initial state is created by executing an ML3 statement:
\begin{lstlisting}
initializeWith(new ExpressionStateBuilder("new Universe"))
\end{lstlisting}
This is only possible for very simple initial states.
\item The initial state is constructed from a JSON representation of a ML3 state:
\begin{lstlisting}
import org.jamesii.ml3.experiment.init.JsonStateBuilder

initializeWith(new JsonStateBuilder("examples/sir/sirstate.json"))
\end{lstlisting}
This does not solve the problem, as a state already has to exist to save it as JSON.
\item The initial state is built by implementing an initial state builder in Scala:
\begin{lstlisting}
import org.jamesii.ml3.experiment.init._
import org.jamesii.ml3.model._
import org.jamesii.ml3.model.agents._
import org.jamesii.ml3.model.state._
import org.apache.commons.math3.random.RandomGenerator

initializeWith(new CSStateBuilder())

class CSStateBuilder extends IInitialStateBuilder {
      def buildInitialState(model: Model, sf: IStateFactory, af: IAgentFactory, rng: RandomGenerator, params: Parameters) : IState = {
      	val state = sf.create()
      	for (i <- 1 until 1000) {
      	  val agent = af.createAgent(model.getAgentDeclaration("Person"), 0)
      	  state.addAgent(agent)
      	}
      	state
      }
  }
\end{lstlisting}
This is very cumbersome and requires some insight into the ML3 simulator.
\end{enumerate}

\subsection{Observation}
The SESSL-ML3 binding supports two kinds of observations: time-triggered and event-triggered observations. Time-triggered observations are made when the simulation reaches a certain time. They allow to make observations of the full model state. Event-triggered observations are made when an agent is subject to a specified event. They allow to observe properties of the agent who underwent the event. To make observations, SESSL needs two informations: when to observe something, and what to observe. To make observations, the trait \lstinline|Observation| must be added to the experiments.

\subsubsection{Time-triggered Observations}
Time-triggered observations are issued as via \lstinline|observeAt|, by providing a list or range of observation points. For example \lstinline|observeAt(range(0, 5, 100))| issues observations every 5 units of time, with the first observation at time 0 and the last observation at time 100. Time-triggered observations allow to observe properties of the whole model state. 

\paragraph{Counting agents.} The ML3 binding allows to observe the number of agents of a certain type, that have a given property. The following snippet shows an example:
\begin{lstlisting}
observeAt(range(0, 1, 150))
observe("persons" ~ agentCount("Person"))
\end{lstlisting}
Here we observe the number of agents of type \texttt{Person}, at every time unit between 0 and 150, and this observation is bound to the name \lstinline|"persons"|. Similarly, one can only count agents who fulfill a certain condition, e.g. , all persons who are currently infected could be observed as:
\begin{lstlisting}
observe("infected" ~ agentCount("Person", "ego.status = 'infected'"))
\end{lstlisting}
The condition is given as ML3 code. Strings in the ML3 code can be marked with single quotes instead of double quotes.

\paragraph{Property distribution.} The distribution of a given property for all agents of a certain type can also be observed:
\begin{lstlisting}
observeAt(range(0, 1, 150))
observe("status" ~ expressionDistribution("Person", "ego.status"))
\end{lstlisting}
Her we observe the status of every person and bind it to the name \lstinline|status|.

\subsubsection{Event-triggered Observations}
To issue event-triggered observations one needs to specify the type of event that shall be observed, and the observation that shall be made. Three types of events can be observed:
\begin{itemize}
\item The creation of agents of a certain type, e.g., of persons:
\begin{lstlisting}
observeAt(Creation("Person"))
\end{lstlisting}
\item The death of agents of a certain type, e.g., of persons:
\begin{lstlisting}
observeAt(Death("Person"))
\end{lstlisting}
\item The change of links or attributes of agents of a certain type, e.g. , the status attribute of persons:
\begin{lstlisting}
observeAt(Change("Person", "status"))
\end{lstlisting}
\end{itemize}

All these observations can be restricted to agents that fulfill a certain property after the event by adding a condition as an additional parameter. E.g., with
\begin{lstlisting}
observeAt(Death("Person", "ego.status = 'infected'"))
\end{lstlisting}
one could observe the death of persons that are infected.

When the event occurs, one can observe properties of the agent that was subject to the event. For example, the following code snippet issues the observation of the age of persons when they die, and binds the results to the name \lstinline|"deathAges"|:
\begin{lstlisting}
observeAt(Death("Person")) {
	observe("deathAges" ~ expression("ego.age"))
}

observeAt(Change("Person", "status", "ego.status = 'infected'")) {
	observe("infectionAge" ~ expression("ego.age"))
}
\end{lstlisting}

\subsection{Storing Results}
Observations can be saved as CSV files. When the trait \lstinline|CSVOutput| is added to the experiment, simply use:
\begin{lstlisting}
withExperimentResult(writeCSV)
\end{lstlisting}
Results are stored in the directory \texttt{results}.

\section{Known Issues}
A constantly updated list of known issues can be found at \url{https://git.informatik.uni-rostock.de/mosi/ml3-sandbox/issues}. Please feel free to notify the author about any additional issues you encounter.

\bibliography{ml3-sandbox} 
\bibliographystyle{plain}

\end{document}