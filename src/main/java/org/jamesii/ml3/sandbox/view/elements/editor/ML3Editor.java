/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.editor;

import org.jamesii.ml3.sandbox.controller.MainController;
import org.jamesii.ml3.sandbox.view.elements.editor.ml3.ML3EditorPane;
import org.jamesii.ml3.sandbox.view.elements.editor.ml3.ML3TextArea;

import javax.swing.*;
import java.awt.*;

/**
 * Created by andreas on 2/15/17.
 */
public class ML3Editor extends JPanel {
	private final MainController controller;
	private final ML3TextArea modelTextArea;
	private final ML3EditorPane modelTextPane;

	public ML3Editor(MainController controller) {
		super(new BorderLayout());
		this.controller = controller;
		this.modelTextArea = new ML3TextArea();
		this.modelTextPane = new ML3EditorPane(this.modelTextArea);
		this.setup();
	}

	private void setup() {
		this.add(this.modelTextPane, BorderLayout.CENTER);
	}

	public ML3TextArea getTextArea() {
		return modelTextArea;
	}
}
