/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

/**
 * Created by andreas on 2/15/17.
 */
public class LogPane extends JTextPane implements MouseWheelListener {
	public LogPane() {
		super();
		this.setup();
	}

	private void setup() {
		this.setEditable(false);
		this.addMouseWheelListener(this);
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if (e.isControlDown()) {
			if (e.getWheelRotation() < 0) {
				Font font = getFont();
				float newSESSLSize = Math.min(72, font.getSize() + 1);
				setFont(font.deriveFont(newSESSLSize));
			} else {
				Font font = getFont();
				float newSESSLSize = Math.max(1, font.getSize() - 1);
				setFont(font.deriveFont(newSESSLSize));
			}
		} else {
			// pass the event on to the scroll pane
			getParent().dispatchEvent(e);
		}
	}
}
