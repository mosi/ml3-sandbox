/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.menubar;

import org.jamesii.ml3.sandbox.controller.MainController;
import org.jamesii.ml3.sandbox.view.elements.menubar.menu.ExperimentMenu;
import org.jamesii.ml3.sandbox.view.elements.menubar.menu.MainMenu;
import org.jamesii.ml3.sandbox.view.elements.menubar.menu.ModelMenu;

import javax.swing.*;

/**
 * Created by andreas on 2/14/17.
 */
public class MenuBar extends JMenuBar {


	private final MainMenu mainMenu;
	private final ModelMenu modelMenu;
	private final ExperimentMenu experimentMenu;

	public MenuBar(MainController controller) {
		this.mainMenu = new MainMenu(controller);
		this.modelMenu = new ModelMenu(controller);
		this.experimentMenu = new ExperimentMenu(controller);
		this.setup();
	}

	private void setup() {
		this.add(mainMenu);
		this.add(modelMenu);
		this.add(experimentMenu);
	}
}
