/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements;

import org.jamesii.ml3.sandbox.controller.MainController;
import org.jamesii.ml3.sandbox.view.elements.editor.ML3Editor;
import org.jamesii.ml3.sandbox.view.elements.editor.SesslEditor;

import javax.swing.*;
import java.awt.*;

/**
 * Created by andreas on 2/15/17.
 */
public class MainPanel extends JPanel {
	private final LogPane logPane;
	private MainController controller;
	private JSplitPane logEditorSplit;
	private JSplitPane ml3SesslSplit;
	private JScrollPane logScrollPane;

	public ML3Editor getMl3Editor() {
		return ml3Editor;
	}

	public SesslEditor getSesslEditor() {
		return sesslEditor;
	}

	private ML3Editor ml3Editor;
	private SesslEditor sesslEditor;

	public MainPanel(MainController controller) {
		super(new BorderLayout());
		this.controller = controller;
		this.logPane = new LogPane();
		this.logEditorSplit = new JSplitPane();
		this.ml3SesslSplit = new JSplitPane();
		this.ml3Editor = new ML3Editor(controller);
		this.sesslEditor = new SesslEditor(controller);
		this.controller = controller;
		logScrollPane = new JScrollPane(logPane);
		this.setup();
	}

	private void setup() {
		// setup logEditorSplit
		this.logEditorSplit.setOrientation(JSplitPane.VERTICAL_SPLIT);
		this.logEditorSplit.setResizeWeight(0.8);
		this.logEditorSplit.setTopComponent(this.ml3SesslSplit);
		this.logEditorSplit.setBottomComponent(this.logScrollPane);

		// setup the split between editors
		ml3SesslSplit.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
		ml3SesslSplit.setResizeWeight(.5);
		ml3SesslSplit.setLeftComponent(ml3Editor);
		ml3SesslSplit.setRightComponent(sesslEditor);

		logScrollPane.setBorder(BorderFactory.createTitledBorder("Log"));

		this.add(this.logEditorSplit, BorderLayout.CENTER);
	}

	public LogPane getLogPane() {
		return logPane;
	}
}