/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.menubar.menu;

import org.jamesii.ml3.sandbox.controller.MainController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/**
 * Created by andreas on 2/14/17.
 */
public class MainMenu extends JMenu {
	private final MainController controller;

	public MainMenu(MainController controller) {
		this.controller = controller;
		this.setup();
	}

	private void setup() {
		JMenuItem saveAllItem = new JMenuItem("Save All");
		saveAllItem.addActionListener(e -> {
			controller.getMl3Controller().save();
			controller.getExperimentController().save();
		});
		JMenuItem closeItem = new JMenuItem("Exit");
		closeItem.addActionListener(e -> System.exit(0));


		saveAllItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));

		this.add(saveAllItem);
		this.addSeparator();
		this.add(closeItem);

		this.setText("Main");
	}
}
