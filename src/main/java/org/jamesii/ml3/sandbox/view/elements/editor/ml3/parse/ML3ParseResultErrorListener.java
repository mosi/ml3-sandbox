/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.editor.ml3.parse;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import org.fife.ui.rsyntaxtextarea.RSyntaxDocument;
import org.fife.ui.rsyntaxtextarea.parser.DefaultParseResult;
import org.fife.ui.rsyntaxtextarea.parser.DefaultParserNotice;
import org.fife.ui.rsyntaxtextarea.parser.ParseResult;
import org.fife.ui.rsyntaxtextarea.parser.ParserNotice;

import javax.swing.text.Element;
import java.awt.*;
import java.util.BitSet;

/**
 * @author Oliver Reinhardt
 */
public class ML3ParseResultErrorListener implements ANTLRErrorListener {
	private DefaultParseResult result;
	private ML3ErrorParser parser;
	private RSyntaxDocument doc;

	public ML3ParseResultErrorListener(ML3ErrorParser parser, RSyntaxDocument doc) {
		this.parser = parser;
		this.result = new DefaultParseResult(parser);
		this.doc = doc;
		result.setParsedLines(0, doc.getDefaultRootElement().getElementCount() - 1);
	}

	public ParseResult getResult() {
		return result;
	}

	@Override
	public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
		int realLine = line - 1;
		Element root = doc.getDefaultRootElement();
		Element elem = root.getElement(realLine);
		int offset = elem.getStartOffset() + charPositionInLine;
		int length = ((Token) offendingSymbol).getText().length();
		DefaultParserNotice notice = new DefaultParserNotice(parser, msg, line, offset, length);
		notice.setLevel(ParserNotice.Level.ERROR);
		notice.setShowInEditor(true);
		notice.setColor(Color.RED);

		result.addNotice(notice);
	}

	@Override
	public void reportAmbiguity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, boolean exact, BitSet ambigAlts, ATNConfigSet configs) {

	}

	@Override
	public void reportAttemptingFullContext(Parser recognizer, DFA dfa, int startIndex, int stopIndex, BitSet conflictingAlts, ATNConfigSet configs) {

	}

	@Override
	public void reportContextSensitivity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, int prediction, ATNConfigSet configs) {

	}
}
