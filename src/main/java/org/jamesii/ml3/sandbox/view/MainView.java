/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view;

import org.jamesii.ml3.sandbox.controller.MainController;
import org.jamesii.ml3.sandbox.view.elements.MainPanel;
import org.jamesii.ml3.sandbox.view.elements.menubar.MenuBar;

import javax.swing.*;
import java.awt.*;
import java.io.PrintStream;

/**
 * Created by andreas on 2/14/17.
 */
public class MainView extends JFrame {
	private final MenuBar menuBar;
	private final MainPanel mainPanel;

	public String getMl3FileName() {
		return ml3FileName;
	}

	public void setMl3FileName(String ml3FileName) {
		this.ml3FileName = ml3FileName;
		setTitle();
	}

	public String getSesslFileName() {
		return sesslFileName;
	}

	public void setSesslFileName(String sesslFileName) {
		this.sesslFileName = sesslFileName;
		setTitle();
	}

	public void unsetML3FileName() {
		setMl3FileName("none");
	}

	public void unsetSesslFileName() {
		setSesslFileName("none");
	}

	private String ml3FileName;
	private String sesslFileName;

	private MainController controller;

	public MainView(MainController controller) {
		this.menuBar = new org.jamesii.ml3.sandbox.view.elements.menubar.MenuBar(controller);
		this.mainPanel = new MainPanel(controller);
	}

	public void setController(MainController controller) {
		this.controller = controller;
	}

	public void run() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				setup();
			}
		});
		this.setVisible(true);
	}

	private void setup() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1024, 768);
		this.setResizable(true);
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		// setup menuBar
		this.setJMenuBar(this.menuBar);
		// setup general structure
		this.add(this.mainPanel);

		unsetML3FileName();
		unsetSesslFileName();

		System.setOut(new PrintStream(new TextPaneOutputStream(mainPanel.getLogPane(), Color.BLACK)));
		System.setErr(new PrintStream(new TextPaneOutputStream(mainPanel.getLogPane(), Color.RED)));
	}

	public MainPanel getMainPanel() {
		return mainPanel;
	}

	private void setTitle() {
		String title = "ML3 Sandbox  < " + ml3FileName + " | " + sesslFileName + " >";
		setTitle(title);
	}
}
