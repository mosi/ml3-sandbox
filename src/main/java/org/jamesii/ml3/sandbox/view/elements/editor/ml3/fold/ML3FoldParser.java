/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.editor.ml3.fold;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.folding.Fold;
import org.fife.ui.rsyntaxtextarea.folding.FoldParser;
import org.jamesii.ml3.parser.antlr4.ML3Lexer;
import org.jamesii.ml3.parser.antlr4.ML3Parser;

import java.util.List;

/**
 * @author Oliver Reinhardt
 */
public class ML3FoldParser implements FoldParser {
	@Override
	public List<Fold> getFolds(RSyntaxTextArea textArea) {
		ML3FoldListener listener = new ML3FoldListener(textArea);
		ML3Lexer lexer = new ML3Lexer(new ANTLRInputStream(textArea.getText()));
		ML3Parser parser = new ML3Parser(new CommonTokenStream(lexer));
		parser.addParseListener(listener);
		parser.removeErrorListeners();
		parser.model();
		return listener.getFolds();
	}
}
