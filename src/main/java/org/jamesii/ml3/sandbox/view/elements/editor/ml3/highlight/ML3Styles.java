/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.editor.ml3.highlight;

import org.fife.ui.rsyntaxtextarea.Style;

import java.awt.*;

/**
 * @author Oliver Reinhardt
 */
public class ML3Styles {
	public static Style DEFAULT;
	public static Style COMMENT;
	public static Style STRING;
	public static Style ERROR;
	public static Style KEYWORD;
	public static Style AGENT_TYPE;
	public static Style IDENTIFIER;
	public static Style LOCAL_NAME;

	public static Font font = new Font(Font.MONOSPACED, Font.PLAIN, 12);
	public static Font bold = font.deriveFont(Font.BOLD);


	static {
		update(new Font(Font.MONOSPACED, Font.PLAIN, 12));
	}

	public static void update(Font newBaseFont) {
		font = newBaseFont;
		bold = font.deriveFont(Font.BOLD);

		DEFAULT = new Style();
		DEFAULT.foreground = Color.BLACK;
		DEFAULT.font = font;


		COMMENT = (Style) DEFAULT.clone();
		COMMENT.foreground = new Color(63, 127, 95);

		STRING = (Style) DEFAULT.clone();
		STRING.foreground = new Color(0, 0, 202);

		ERROR = (Style) DEFAULT.clone();
		ERROR.foreground = Color.RED;

		KEYWORD = (Style) DEFAULT.clone();
		KEYWORD.foreground = new Color(149, 0, 85);
		KEYWORD.font = bold;

		AGENT_TYPE = (Style) DEFAULT.clone();

		IDENTIFIER = (Style) DEFAULT.clone();

		LOCAL_NAME = (Style) DEFAULT.clone();
	}
}
