/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.editor.ml3.parse;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.fife.ui.rsyntaxtextarea.RSyntaxDocument;
import org.fife.ui.rsyntaxtextarea.parser.AbstractParser;
import org.fife.ui.rsyntaxtextarea.parser.ParseResult;
import org.jamesii.ml3.parser.antlr4.ML3Lexer;
import org.jamesii.ml3.parser.antlr4.ML3Parser;

import javax.swing.text.BadLocationException;

/**
 * @author Oliver Reinhardt
 */
public class ML3ErrorParser extends AbstractParser {
	@Override
	public ParseResult parse(RSyntaxDocument doc, String style) {
		ML3ParseResultErrorListener listener = new ML3ParseResultErrorListener(this, doc);
		ML3Lexer lexer = null;
		try {
			lexer = new ML3Lexer(new ANTLRInputStream(doc.getText(0, doc.getLength())));
			ML3Parser parser = new ML3Parser(new CommonTokenStream(lexer));
			parser.removeErrorListeners();
			parser.addErrorListener(listener);
			parser.model();
		} catch (BadLocationException e) {
			e.printStackTrace();
		}

		return listener.getResult();
	}
}
