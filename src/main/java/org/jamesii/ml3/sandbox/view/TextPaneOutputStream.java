/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;
import java.io.IOException;
import java.io.OutputStream;

public class TextPaneOutputStream extends OutputStream {
	private JTextPane ta;
	private AttributeSet aset;

	public TextPaneOutputStream(JTextPane ta, Color color) {
		this.ta = ta;
		StyleContext sc = StyleContext.getDefaultStyleContext();
		aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, color);
	}

	@Override
	public void write(int c) throws IOException {
		ta.setCharacterAttributes(aset, false);

		Document doc = ta.getDocument();
		try {
			doc.insertString(doc.getLength(), String.valueOf((char) c), aset);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}

		ta.setCaretPosition(ta.getDocument().getLength());
	}

}
