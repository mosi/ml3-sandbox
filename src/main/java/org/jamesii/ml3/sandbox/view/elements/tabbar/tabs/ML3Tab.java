/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.tabbar.tabs;

import org.jamesii.ml3.sandbox.controller.MainController;
import org.jamesii.ml3.sandbox.view.elements.editor.ML3Editor;

import javax.swing.*;
import java.awt.*;

/**
 * Created by andreas on 2/15/17.
 */
public class ML3Tab extends JPanel {
	private final MainController controller;
	private final ML3Editor editor;

	public ML3Tab(MainController controller) {
		super(new BorderLayout());
		this.controller = controller;
		this.editor = new ML3Editor(controller);
		this.setup();
	}

	public ML3Editor getEditor() {
		return editor;
	}

	private void setup() {
		this.add(editor, BorderLayout.CENTER);
	}
}
