/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.editor;


import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.jamesii.ml3.sandbox.controller.MainController;
import org.jamesii.ml3.sandbox.view.elements.editor.sessl.SesslEditorPane;
import org.jamesii.ml3.sandbox.view.elements.editor.sessl.SesslTextArea;

import javax.swing.*;
import java.awt.*;

/**
 * Created by andreas on 2/15/17.
 */
public class SesslEditor extends JPanel {
	private final MainController controller;
	private final SesslTextArea editorText;
	private final SesslEditorPane editorPane;

	public SesslEditor(MainController controller) {
		super(new BorderLayout());
		this.controller = controller;
		this.editorText = new SesslTextArea();
		this.editorPane = new SesslEditorPane(this.editorText);
		this.setup();
	}

	private void setup() {
		this.editorText.setCodeFoldingEnabled(true);
		this.editorText.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SCALA);
		this.add(this.editorPane, BorderLayout.CENTER);
	}

	public SesslTextArea getTextArea() {
		return editorText;
	}
}
