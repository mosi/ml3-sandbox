/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.tabbar;

import org.jamesii.ml3.sandbox.controller.MainController;
import org.jamesii.ml3.sandbox.view.elements.tabbar.tabs.ExperimentTab;
import org.jamesii.ml3.sandbox.view.elements.tabbar.tabs.ML3Tab;

import javax.swing.*;

/**
 * Created by andreas on 2/15/17.
 */
public class EditorTabBar extends JTabbedPane {
	private final MainController controller;
	private ML3Tab ml3Tab;

	public EditorTabBar(MainController controller) {
		this.controller = controller;
		this.setup();
	}

	private void setup() {
		this.addML3Tab();

	}

	public void addML3Tab() {
		ml3Tab = new ML3Tab(this.controller);
		this.add("Model", ml3Tab);
	}

	public void addExperimentTab() {
		this.add("Experiment", new ExperimentTab(this.controller));

	}

	public ML3Tab getMl3Tab() {
		return ml3Tab;
	}
}
