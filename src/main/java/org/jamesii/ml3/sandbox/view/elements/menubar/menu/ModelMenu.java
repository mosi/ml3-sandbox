/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.menubar.menu;

import org.jamesii.ml3.sandbox.controller.MainController;

import javax.swing.*;

/**
 * @author Oliver Reinhardt
 */
public class ModelMenu extends JMenu {
	private final MainController controller;

	public ModelMenu(MainController controller) {
		this.controller = controller;

		this.setup();
	}

	private void setup() {
		JMenuItem newModelItem = new JMenuItem("New");
		newModelItem.addActionListener(e -> controller.getMl3Controller().newFile());
		JMenuItem openModelItem = new JMenuItem("Open...");
		openModelItem.addActionListener(e -> controller.getMl3Controller().open());
		JMenuItem saveModelItem = new JMenuItem("Save");
		saveModelItem.addActionListener(e -> controller.getMl3Controller().save());
		JMenuItem saveModelAsItem = new JMenuItem("Save As...");
		saveModelAsItem.addActionListener(e -> controller.getMl3Controller().saveAs());

		this.add(newModelItem);
		this.add(openModelItem);
		this.add(saveModelItem);
		this.add(saveModelAsItem);
		this.setText("Model");
	}
}
