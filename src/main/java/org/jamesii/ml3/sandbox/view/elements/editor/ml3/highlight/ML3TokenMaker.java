/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.editor.ml3.highlight;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rsyntaxtextarea.TokenImpl;
import org.fife.ui.rsyntaxtextarea.TokenMakerBase;
import org.jamesii.ml3.parser.antlr4.ML3Lexer;

import javax.swing.text.Segment;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Oliver Reinhardt
 */
public class ML3TokenMaker extends TokenMakerBase {
	@Override
	public Token getTokenList(Segment text, int initialTokenType, int startOffset) {
		ML3Lexer lexer = new ML3Lexer(new ANTLRInputStream(text.toString()));
		List<org.antlr.v4.runtime.Token> tokens = new ArrayList<>();

		while (!lexer._hitEOF) {
			tokens.add(lexer.nextToken());
		}

		return getToken(text, startOffset, tokens);
	}

	private Token getToken(Segment text, int startOffset, List<org.antlr.v4.runtime.Token> antlrTokens) {
		if (antlrTokens.isEmpty()) return null;
		org.antlr.v4.runtime.Token antlrToken = antlrTokens.get(0);
		TokenImpl token = new TokenImpl(text, text.offset + antlrToken.getStartIndex(), text.offset + antlrToken.getStartIndex() + antlrToken.getText().length() - 1, startOffset + antlrToken.getStartIndex(), antlrToken.getType(), 0);
		token.setNextToken(getToken(text, startOffset, antlrTokens.subList(1, antlrTokens.size())));
		return token;
	}
}
