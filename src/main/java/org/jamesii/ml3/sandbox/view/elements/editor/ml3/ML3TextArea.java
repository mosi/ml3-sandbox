/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.editor.ml3;

import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.TokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.folding.FoldParserManager;
import org.jamesii.ml3.sandbox.view.elements.editor.ml3.fold.ML3FoldParser;
import org.jamesii.ml3.sandbox.view.elements.editor.ml3.highlight.ML3Styles;
import org.jamesii.ml3.sandbox.view.elements.editor.ml3.highlight.ML3SyntaxScheme;
import org.jamesii.ml3.sandbox.view.elements.editor.ml3.parse.ML3ErrorParser;

import java.awt.*;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

/**
 * @author Oliver Reinhardt
 */
public class ML3TextArea extends RSyntaxTextArea implements MouseWheelListener {

	static {
		AbstractTokenMakerFactory atmf = (AbstractTokenMakerFactory) TokenMakerFactory.getDefaultInstance();
		atmf.putMapping("ml3", "org.jamesii.ml3.sandbox.view.elements.editor.ml3.highlight.ML3TokenMaker");
		FoldParserManager.get().addFoldParserMapping("ml3", new ML3FoldParser());
	}

	public ML3TextArea() {
		super();

		this.setSyntaxEditingStyle("ml3");
		this.setSyntaxScheme(new ML3SyntaxScheme(true));
		this.setLineWrap(true);
		this.setWrapStyleWord(true);
		this.setFont(ML3Styles.font);
		this.setCodeFoldingEnabled(true);
		this.addParser(new ML3ErrorParser());
		this.setParserDelay(500);
		this.addMouseWheelListener(this);
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if (e.isControlDown()) {
			if (e.getWheelRotation() < 0) {
				Font ml3Font = getFont();
				float newML3Size = Math.min(72, ml3Font.getSize() + 1);
				Font newML3Font = ml3Font.deriveFont(newML3Size);
				setFont(newML3Font);
				ML3Styles.update(newML3Font);
			} else {
				Font ml3Font = getFont();
				float newML3Size = Math.max(1, ml3Font.getSize() - 1);
				Font newML3Font = ml3Font.deriveFont(newML3Size);
				setFont(newML3Font);
				ML3Styles.update(newML3Font);
			}
		} else {
			// pass the event on to the scroll pane
			getParent().dispatchEvent(e);
		}
	}
}
