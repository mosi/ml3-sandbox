/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.editor.ml3.fold;

import org.antlr.v4.runtime.ParserRuleContext;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.folding.Fold;
import org.fife.ui.rsyntaxtextarea.folding.FoldType;
import org.jamesii.ml3.parser.antlr4.ML3BaseListener;
import org.jamesii.ml3.parser.antlr4.ML3Parser;

import javax.swing.text.BadLocationException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Oliver Reinhardt
 */
public class ML3FoldListener extends ML3BaseListener {
	public static final int RULE = FoldType.FOLD_TYPE_USER_DEFINED_MIN;
	public static final int RULE_BLOCK = RULE + 1;
	public static final int FUNCTION = RULE_BLOCK + 1;
	public static final int PROCEDURE = FUNCTION + 1;
	public static final int AGENT_DECLARATION = PROCEDURE + 1;

	private LinkedList<Fold> folds;
	private RSyntaxTextArea ta;
	private Deque<Fold> stack = new ArrayDeque<>();

	public ML3FoldListener(RSyntaxTextArea ta) {
		this.folds = new LinkedList<>();
		this.ta = ta;
	}

	public List<Fold> getFolds() {
		return folds;
	}

	private void beginFold(int type, ParserRuleContext ctx) {
		try {
			int line = ctx.start.getLine() - 1;
			int offset = ta.getLineStartOffset(line);
			if (!folds.isEmpty() && folds.getLast().getEndOffset() > offset) {
				folds.getLast().createChild(type, offset);
			} else {
				folds.add(new Fold(type, ta, offset));

			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	private void endFold(ParserRuleContext ctx) {
		try {
			int line = ctx.stop.getLine() - 1;
			int offset = ta.getLineStartOffset(line);

			Fold last = folds.getLast();
			while (last.getHasChildFolds() && last.getLastChild().getEndOffset() > offset) {
				last = last.getLastChild();
			}

			last.setEndOffset(offset);

			if (last.getLineCount() < 1) {
				if (last.getParent() != null) {
					last.removeFromParent();
				} else {
					folds.removeLast();
				}
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void enterAgentDec(ML3Parser.AgentDecContext agentDecContext) {
		beginFold(AGENT_DECLARATION, agentDecContext);
	}

	@Override
	public void exitAgentDec(ML3Parser.AgentDecContext agentDecContext) {
		endFold(agentDecContext);
	}

	@Override
	public void enterFuncDec(ML3Parser.FuncDecContext funcDecContext) {
		beginFold(FUNCTION, funcDecContext);
	}

	@Override
	public void exitFuncDec(ML3Parser.FuncDecContext funcDecContext) {
		endFold(funcDecContext);
	}

	@Override
	public void enterProcDec(ML3Parser.ProcDecContext procDecContext) {
		beginFold(PROCEDURE, procDecContext);
	}

	@Override
	public void exitProcDec(ML3Parser.ProcDecContext procDecContext) {
		endFold(procDecContext);
	}

	@Override
	public void enterRuleBlock(ML3Parser.RuleBlockContext ruleBlockContext) {
		beginFold(RULE_BLOCK, ruleBlockContext);
	}

	@Override
	public void exitRuleBlock(ML3Parser.RuleBlockContext ruleBlockContext) {
		endFold(ruleBlockContext);
	}

	@Override
	public void enterRuleDec(ML3Parser.RuleDecContext ruleDecContext) {
		beginFold(RULE, ruleDecContext);
	}

	@Override
	public void exitRuleDec(ML3Parser.RuleDecContext ruleDecContext) {
		endFold(ruleDecContext);
	}
}
