/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.editor.sessl;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

import java.awt.*;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

/**
 * @author Oliver Reinhardt
 */
public class SesslTextArea extends RSyntaxTextArea implements MouseWheelListener {

	public SesslTextArea() {
		super();
		this.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SCALA);
		this.setLineWrap(true);
		this.setWrapStyleWord(true);
		this.setCodeFoldingEnabled(true);
		this.addParser(new SesslErrorParser());
		this.setParserDelay(500);
		this.addMouseWheelListener(this);
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if (e.isControlDown()) {
			if (e.getWheelRotation() < 0) {
				Font sesslFont = getFont();
				float newSESSLSize = Math.min(72, sesslFont.getSize() + 1);
				setFont(sesslFont.deriveFont(newSESSLSize));
			} else {
				Font sesslFont = getFont();
				float newSESSLSize = Math.max(1, sesslFont.getSize() - 1);
				setFont(sesslFont.deriveFont(newSESSLSize));
			}
		} else {
			// pass the event on to the scroll pane
			getParent().dispatchEvent(e);
		}
	}
}
