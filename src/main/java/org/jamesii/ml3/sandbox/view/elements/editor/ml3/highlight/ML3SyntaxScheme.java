/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.editor.ml3.highlight;

import org.fife.ui.rsyntaxtextarea.Style;
import org.fife.ui.rsyntaxtextarea.SyntaxScheme;
import org.jamesii.ml3.parser.antlr4.ML3Lexer;

import java.awt.*;

/**
 * @author Oliver Reinhardt
 */
public class ML3SyntaxScheme extends SyntaxScheme {
	public ML3SyntaxScheme(boolean useDefaults) {
		super(useDefaults);
	}

	public ML3SyntaxScheme(Font baseFont) {
		super(baseFont);
	}

	public ML3SyntaxScheme(Font baseFont, boolean fontStyles) {
		super(baseFont, fontStyles);
	}

	@Override
	public Style getStyle(int index) {
		switch (index) {
			case ML3Lexer.Age:
			case ML3Lexer.All:
			case ML3Lexer.Alter:
			case ML3Lexer.Do:
			case ML3Lexer.Each:
			case ML3Lexer.Ego:
			case ML3Lexer.Else:
			case ML3Lexer.End:
			case ML3Lexer.Every:
			case ML3Lexer.For:
			case ML3Lexer.If:
			case ML3Lexer.In:
			case ML3Lexer.Instantly:
			case ML3Lexer.New:
			case ML3Lexer.Now:
			case ML3Lexer.Synchronized:
			case ML3Lexer.Then:
			case ML3Lexer.Where:
			case ML3Lexer.TypeBool:
			case ML3Lexer.TypeInt:
			case ML3Lexer.TypeString:
			case ML3Lexer.TypeReal:
				return ML3Styles.KEYWORD;
			case ML3Lexer.String:
				return ML3Styles.STRING;
			case ML3Lexer.COMMENT:
			case ML3Lexer.LINE_COMMENT:
				return ML3Styles.COMMENT;
			case ML3Lexer.UNMATCHED:
				return ML3Styles.ERROR;
			case ML3Lexer.AgentIdentifier:
				return ML3Styles.AGENT_TYPE;
			case ML3Lexer.Identifier:
				return ML3Styles.IDENTIFIER;
			case ML3Lexer.LocalIdentifier:
				return ML3Styles.LOCAL_NAME;
			default:
				return ML3Styles.DEFAULT;
		}
	}
}
