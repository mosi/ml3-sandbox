/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.view.elements.editor.sessl;

import org.apache.commons.io.output.NullOutputStream;
import org.fife.ui.rsyntaxtextarea.RSyntaxDocument;
import org.fife.ui.rsyntaxtextarea.parser.*;
import sessl.parser.ExperimentRunner;

import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import java.awt.*;
import java.io.PrintStream;

/**
 * @author Oliver Reinhardt
 */
public class SesslErrorParser extends AbstractParser {
	@Override
	public ParseResult parse(RSyntaxDocument doc, String style) {
		String text = "";
		try {
			text = doc.getText(0, doc.getLength());
		} catch (BadLocationException e) {
			e.printStackTrace();
		}

		String errorMsg = "";
		try {
			// TODO: Workaround to hide the parser's error output
			PrintStream sysErr = System.err;
			System.setErr(new PrintStream(new NullOutputStream()));
			ExperimentRunner runner = new ExperimentRunner();
			if (!runner.isValidExperimentText(text)) {
				errorMsg = runner.lastError();
			}
			System.setErr(sysErr);
		} catch (Exception e) {

		}

		return parseErrorMessage(errorMsg, doc);
	}

	private ParseResult parseErrorMessage(String errorMsg, RSyntaxDocument doc) {
		DefaultParseResult result = new DefaultParseResult(this);

		String[] errors = errorMsg.split("error: line ");

		for (int i = 1; i < errors.length; i++) {
			String error = errors[i];
			int colon = error.indexOf(':');
			String lineString = error.substring(0, colon);
			String msg = error.substring(colon + 1);
			int line = Integer.parseInt(lineString);

			Element elem = doc.getDefaultRootElement().getElement(line - 1);

			DefaultParserNotice notice = new DefaultParserNotice(this, msg, line, elem.getStartOffset(), elem.getEndOffset() - elem.getStartOffset());
			notice.setLevel(ParserNotice.Level.ERROR);
			notice.setShowInEditor(true);
			notice.setColor(Color.RED);

			result.addNotice(notice);
		}

		return result;
	}
}
