/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.controller;


import org.jamesii.ml3.sandbox.view.EditorLog;
import org.jamesii.ml3.sandbox.view.MainView;
import org.jamesii.ml3.sandbox.view.TextPaneOutputStream;

import java.awt.*;
import java.io.PrintStream;

/**
 * Created by andreas on 2/14/17.
 */
public class MainController {
	private final MainView view;
	private ExperimentController experimentController;
	private ML3Controller ml3Controller;
	private EditorLog log;

	public MainController() {
		this.view = new MainView(this);
		this.setupView();
		this.setupControllers();
		this.log = new EditorLog(new PrintStream(new TextPaneOutputStream(view.getMainPanel().getLogPane(), Color.BLACK)));
	}

	private void setupView() {
		this.view.setController(this);
	}

	private void setupControllers() {
		this.experimentController = new ExperimentController(this);
		this.ml3Controller = new ML3Controller(this);
	}


	public void showView() {
		this.view.run();
	}

	public ExperimentController getExperimentController() {
		return experimentController;
	}

	public ML3Controller getMl3Controller() {
		return ml3Controller;
	}

	public MainView getView() {
		return view;
	}
}
