/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.controller;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Created by andreas on 2/14/17.
 */
public class ML3Controller {
	private final MainController controller;

	private File ml3File;

	public ML3Controller(MainController controller) {
		this.controller = controller;
	}

	public void newFile() {
		ml3File = null;
		controller.getView().getMainPanel().getMl3Editor().getTextArea().setText("");
		controller.getView().unsetSesslFileName();
		System.out.println("New model file...");
	}

	public void save() {
		if (ml3File == null) {
			saveAs();
		} else {
			try {
				FileUtils.write(ml3File, controller.getView().getMainPanel().getMl3Editor().getTextArea().getText(), Charset.defaultCharset());
				System.out.println("Saved model file to: " + ml3File.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void saveAs() {
		JFileChooser chooser = new ML3FileChooser(false);
		if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
			ml3File = chooser.getSelectedFile();
			save();
			controller.getView().setMl3FileName(ml3File.getAbsolutePath());
		}
	}

	public void open() {
		JFileChooser chooser = new ML3FileChooser(true);
		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			File selectedFile = chooser.getSelectedFile();
			if (selectedFile != null) {
				try {
					String modelText = FileUtils.readFileToString(selectedFile, Charset.defaultCharset());
					modelText = modelText.replaceAll("\r\n", "\n");
					modelText = modelText.replaceAll("\r", "");
					controller.getView().getMainPanel().getMl3Editor().getTextArea().setText(modelText);
					ml3File = selectedFile;
					System.out.println("Opened model file from: " + ml3File.getAbsolutePath());
					controller.getView().setMl3FileName(ml3File.getAbsolutePath());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static class ML3FileChooser extends JFileChooser {
		public ML3FileChooser(boolean open) {
			super();
			this.setDialogTitle((open ? "Open" : "Save") + " ML3 model");
			this.setFileFilter(new ML3FileFilter());
			this.setCurrentDirectory(new File("."));
		}
	}

	private static class ML3FileFilter extends FileFilter {
		@Override
		public boolean accept(File f) {
			String fileExtension = FilenameUtils.getExtension(f.getName());
			if (fileExtension.equals("ml3")) {
				return true;
			} else if (f.isDirectory()) {
				return true;
			}
			return false;
		}

		@Override
		public String getDescription() {
			return "ML3 model files (.ml3)";
		}
	}

	public boolean hasFile() {
		return ml3File != null;
	}

	public File getFile() {
		return ml3File;
	}
}
