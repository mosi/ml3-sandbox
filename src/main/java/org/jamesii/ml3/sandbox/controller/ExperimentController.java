/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.controller;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutorService;

/**
 * Created by andreas on 2/14/17.
 */
public class ExperimentController {
	private final MainController controller;

	private File sesslFile;

	private ExecutorService executor;

	private Process process;

	public ExperimentController(MainController controller) {
		this.controller = controller;

		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			if (process != null && process.isAlive()) {
				process.destroyForcibly();
			}
		}));
	}

	public void newFile() {
		sesslFile = null;
		controller.getView().getMainPanel().getSesslEditor().getTextArea().setText("");
		controller.getView().unsetSesslFileName();
		System.out.println("New experiment file...");
	}

	public void save() {
		if (sesslFile == null) {
			saveAs();
		} else {
			try {
				FileUtils.write(sesslFile, controller.getView().getMainPanel().getSesslEditor().getTextArea().getText(), Charset.defaultCharset());
				System.out.println("Saved experiment file to: " + sesslFile.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void saveAs() {
		JFileChooser chooser = new SesslFileChooser(false);
		if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
			sesslFile = chooser.getSelectedFile();
			save();
			controller.getView().setMl3FileName(sesslFile.getAbsolutePath());
		}
	}

	public void open() {
		JFileChooser chooser = new SesslFileChooser(true);
		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			File selectedFile = chooser.getSelectedFile();
			if (selectedFile != null) {
				try {
					String modelText = FileUtils.readFileToString(selectedFile, Charset.defaultCharset());
					modelText = modelText.replaceAll("\r\n", "\n");
					modelText = modelText.replaceAll("\r", "");
					controller.getView().getMainPanel().getSesslEditor().getTextArea().setText(modelText);
					sesslFile = selectedFile;
					System.out.println("Opened experiment file from: " + sesslFile.getAbsolutePath());
					controller.getView().setSesslFileName(sesslFile.getAbsolutePath());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static class SesslFileChooser extends JFileChooser {
		public SesslFileChooser(boolean open) {
			super();
			this.setDialogTitle((open ? "Open" : "Save") + " SESSL experiment");
			this.setFileFilter(new SesslFileFilter());
			this.setCurrentDirectory(new File("."));
		}
	}

	private static class SesslFileFilter extends FileFilter {
		@Override
		public boolean accept(File f) {
			String fileExtension = FilenameUtils.getExtension(f.getName());
			if (fileExtension.equals("scala")) {
				return true;
			} else if (f.isDirectory()) {
				return true;
			}
			return false;
		}

		@Override
		public String getDescription() {
			return "SESSL experiment files (.scala)";
		}
	}

	public boolean hasFile() {
		return sesslFile != null;
	}

	public File getFile() {
		return sesslFile;
	}

	public void run() {


		// save model and experiment
		save();
		controller.getMl3Controller().save();

        /*
		System.out.println("set user.dir to " + sesslFile.getParent());
		System.setProperty("user.dir", sesslFile.getParent());

		System.out.println("user dir: " + new File(".").getAbsolutePath());
*/

        /*
		if (executor != null) executor.shutdownNow();
		executor = Executors.newSingleThreadExecutor();
		executor.submit(() -> {
			try {
				ExperimentRunner runner = new ExperimentRunner();
				runner.runML3Experiment(controller.getMl3Controller().getFile(), getFile());
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		executor.shutdown();
		*/

		if (process != null && process.isAlive()) {
			System.out.println("Another experiment is still running. Wait until it is finished or abort it, before starting a new experiment.");
			return;
		}

		System.out.println("Start experiment...");
		try {
			process = SESSLExperiment.exec(controller.getMl3Controller().getFile(), getFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void abort() {
		if (process != null && process.isAlive()) {
			try {
				process.destroyForcibly().waitFor();
				System.out.println("Aborted experiment.");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("No experiment running.");
		}
	}
}
