/*
 * Copyright 2017 University of Rostock
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jamesii.ml3.sandbox.controller;

import org.apache.commons.io.IOUtils;
import sessl.parser.ExperimentRunner;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Oliver Reinhardt
 */
public abstract class SESSLExperiment {
	public static void main(String[] args) throws InterruptedException {
		File model = new File(args[0]);
		File experiment = new File(args[1]);

		try {
			ExperimentRunner runner = new ExperimentRunner();
			runner.runML3Experiment(model, experiment);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static Process exec(File model, File experiment) throws IOException {
		Class clazz = SESSLExperiment.class;
		String javaHome = System.getProperty("java.home");
		String javaBin = javaHome +
				File.separator + "bin" +
				File.separator + "java";
		String classpath = System.getProperty("java.class.path");
		String className = clazz.getCanonicalName();

		ProcessBuilder builder = new ProcessBuilder(
				javaBin, "-cp", classpath, className, model.getAbsolutePath(), experiment.getAbsolutePath());

		Process process = builder.start();

		InputStream processOut = process.getInputStream();
		InputStream processErr = process.getErrorStream();

		ExecutorService executor = Executors.newFixedThreadPool(2);
		executor.execute(() -> {
			try {
				IOUtils.copy(processOut, System.out);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		executor.execute(() -> {
			try {
				IOUtils.copy(processErr, System.err);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		executor.shutdown();

		return process;
	}
}
