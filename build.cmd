if exist build rmdir build /s

call mvn clean package

xcopy target\lib build\lib /e /i /h
xcopy target\ml3-sandbox-*.jar build
xcopy examples build\examples /e /i /h
xcopy doc\ml3-sandbox.pdf build