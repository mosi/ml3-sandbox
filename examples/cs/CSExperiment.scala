import org.apache.commons.math3.random.RandomGenerator
import org.jamesii.ml3.experiment.init._
import org.jamesii.ml3.model._
import org.jamesii.ml3.model.agents._
import org.jamesii.ml3.model.state._
import sessl._
import sessl.ml3._

new Experiment with Observation with ParallelExecution with ParameterMaps with CSVOutput {
  simulator = NextReactionMethod()
  parallelThreads = -1
  replications = 1
  initializeWith(new CSStateBuilder())
  startTime = 0
  stopTime = 10

  observeAt(range(0, 0.1, 10))
  observe("a" ~ agentCount("Person", "ego.a"))
  observe("b" ~ agentCount("Person", "ego.b"))
  observe("c" ~ agentCount("Person", "ego.c"))
  observe("d" ~ agentCount("Person", "ego.d"))
  observe("e" ~ agentCount("Person", "ego.e"))

  withExperimentResult(writeCSV)

  class CSStateBuilder extends IInitialStateBuilder {
    def buildInitialState(model: Model, sf: IStateFactory, af: IAgentFactory, rng: RandomGenerator, params: Parameters): IState = {
      val state = sf.create()
      for (i <- 1 until 1000) {
        val agent = af.createAgent(model.getAgentDeclaration("Person"), 0)
        state.addAgent(agent)
      }
      state
    }
  }

}