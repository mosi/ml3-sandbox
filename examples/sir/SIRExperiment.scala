import org.jamesii.ml3.experiment.init.JsonStateBuilder
import sessl._
import sessl.ml3._

new Experiment with Observation with ParallelExecution with ParameterMaps with CSVOutput {
  simulator = NextReactionMethod()
  parallelThreads = -1
  replications = 10

  initializeWith(new JsonStateBuilder("examples/sir/sirstate.json"))
  startTime = 0
  stopTime = 150

  fromFile("examples/sir/infectionScaling.csv")("infectionScaling")
  set("a" <~ 0.03)
  set("b" <~ 0.05)

  observeAt(range(0, 1, 150))
  observe("susceptible" ~ agentCount("Person", "ego.status = 'susceptible'"))
  observe("infected" ~ agentCount("Person", "ego.status = 'infected'"))
  observe("recovered" ~ agentCount("Person", "ego.status = 'recovered'"))

  withExperimentResult(writeCSV)
}