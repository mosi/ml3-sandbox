import org.jamesii.ml3.experiment.init.JsonStateBuilder
import sessl._
import sessl.ml3._

new Experiment with Observation with ParallelExecution with ParameterMaps with CSVOutput with CentralCompositeDesign {
  simulator = NextReactionMethod()
  parallelThreads = -1
  replications = 5

  initializeWith(new JsonStateBuilder("examples/sir/sirstate.json"))
  startTime = 0
  stopTime = 150

  fromFile("examples/sir/infectionScaling.csv")("infectionScaling")
  // this doesn't make much sense, as central composite 
  // with 2 factors is essentially full factorial
  centralComposite("a" <~ interval(0.0, 0.1), "b" <~ interval(0.0, 1.0))

  observeAt(range(0, 1, 150))
  observe("susceptible" ~ agentCount("Person", "ego.status = 'susceptible'"))
  observe("infected" ~ agentCount("Person", "ego.status = 'infected'"))
  observe("recovered" ~ agentCount("Person", "ego.status = 'recovered'"))

  withExperimentResult(writeCSV)
}