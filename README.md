# The Official ML3 Sandbox Repository

ML3 is an external domain-specific modeling language for continuous-time agent-based models in demography. The latest version of ML3 is available at the [ML3 repository](https://git.informatik.uni-rostock.de/mosi/ml3).

The ML3 Sandbox is a simple editor that allows to edit ML3 models and execute [SESSL](https://git.informatik.uni-rostock.de/mosi/sessl) experiments with these models.

Currently the you will be unable to build the ML3 Sandbox, as some dependencies have not yet been made public. However, the latest version is available [here](https://git.informatik.uni-rostock.de/mosi/ml3-sandbox/tags).

# License

This software is published under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt).